; This drupal-org.make file downloads contrib modules, themes, and 3rd party libraries.

api = 2
core = 7.x
projects[] = drupal

; @see https://www.drupal.org/node/1934086#comment-7137850
projects[drupal][patch][] = "http://drupal.org/files/drupal-1934086-path_load_order-1.patch"

; @see https://www.drupal.org/node/1875824#comment-7420862
projects[drupal][patch][] = "http://drupal.org/files/core-1875824-3-d7.patch"

; @see https://www.drupal.org/node/1903010#comment-8415695
projects[drupal][patch][] = "http://drupal.org/files/issues/drupal-undefinedindex_fileupload-1903010-4.patch"

; @see https://www.drupal.org/node/2264739
projects[drupal][patch][] = "http://drupal.org/files/issues/death-to-tabledrag.patch"

; @see https://www.drupal.org/node/1783278#comment-9735933
projects[drupal][patch][] = "https://www.drupal.org/files/issues/schema-relative-1783278-17-D7-do-not-test.patch"

; Patches should now all be handled by patches.make file thanks to https://bitbucket.org/davereid/drush-patchfile.


; Modules

; Contributed modules

projects[] = ace_editor
projects[] = admin_menu
projects[] = admin_views
projects[adminimal_admin_menu][version] = 1.7
;https://www.drupal.org/node/2407007
projects[adminimal_admin_menu][patch][] = "https://www.drupal.org/files/issues/adminimal_admin_menu-js-error-2407007-3.patch"
projects[asset][version] = 1.0-beta4
projects[] = ckeditor
; @see https://www.drupal.org/node/2480503#comment-10332267
projects[asset][patch][] = "https://www.drupal.org/files/issues/2480503-1-asset-add_more_icons-7.x-1.0-beta4.patch"
; @see https://www.drupal.org/node/2174613#comment-10509492
projects[asset][patch][] = "https://www.drupal.org/files/issues/asset-youtube-video-2174613-6--beta4.patch"
; @see https://www.drupal.org/node/2612602#comment-10550910
projects[asset][patch][] = https://www.drupal.org/files/issues/2612602-3-asset-inline_entity_form_integration.patch
projects[] = chosen
projects[] = coffee
; @see https://www.drupal.org/node/2463187#comment-9779355
projects[ckeditor][patch][] = "https://www.drupal.org/files/issues/allow-custom-icon-paths-2463187-1.patch"
projects[] = block_class
projects[] = ctools
projects[] = context
projects[] = calendar
projects[] = date
projects[] = devel
projects[] = diff
projects[] = ds
projects[] = empty_front_page
projects[] = entity
projects[] = entity_view_mode
projects[] = entitycache
projects[] = entityreference
projects[] = environment_indicator
projects[] = exclude_node_title
projects[] = features
projects[] = features_extra
projects[] = field_collection
projects[] = field_group
projects[] = field_help_helper
projects[] = file_entity
projects[] = fontawesome
projects[] = google_analytics
projects[] = globalredirect
projects[] = imce
projects[] = image_class
projects[] = imagecache_token
projects[] = inline_entity_form
projects[] = jquery_update
projects[] = libraries
projects[] = link
projects[] = linkit
projects[] = logo_block
projects[] = maxlength
projects[] = media
projects[] = menu_admin_per_menu
projects[] = menu_attributes
projects[] = menu_block
projects[] = menu_expanded
projects[] = metatag
projects[] = module_filter
projects[] = node_export
projects[] = nodequeue
projects[] = outdatedbrowser
projects[] = panels
projects[] = panelizer
projects[paragraphs][version] = "1.0-rc4"
projects[] = pathauto
projects[] = rules
projects[] = redirect
projects[] = responsive_menus
projects[] = retina_images
projects[] = strongarm
projects[] = colorbox
projects[] = slick
projects[] = slick_views
projects[slick_extras][version] = 2.0-beta4
projects[] = styleguide
projects[] = menu_firstchild
projects[] = tb_megamenu
; @see https://www.drupal.org/node/2285435
projects[tb_megamenu][patch][] = "https://www.drupal.org/files/issues/tb_megamenu-closing_menu_on_touch-2285435-19.patch"
projects[] = token
projects[] = uuid
projects[] = xmlsitemap
projects[] = views
projects[] = views_bulk_operations
projects[] = views_slideshow
projects[] = webform
projects[] = wysiwyg


; Libraries
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.7.1/ckeditor_4.7.1_full.zip"
libraries[colorbox][download][type] = "get"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/1.x.zip"
libraries[colorbox][directory_name] = colorbox
libraries[chosen][download][type] = "get"
libraries[chosen][download][url] = "https://github.com/harvesthq/chosen/releases/download/1.4.2/chosen_v1.4.2.zip"
libraries[sidr][download][type] = "get"
libraries[sidr][download][url] = "https://github.com/artberri/sidr-package/archive/1.2.1.zip"
libraries[slick][download][type] = git
libraries[slick][download][url] = https://github.com/kenwheeler/slick.git
libraries[slick][directory_name] = slick
libraries[easing][download][type] = git
libraries[easing][download][url] = https://github.com/gdsmith/jquery.easing.git
libraries[easing][directory_name] = easing
libraries[ace][download][type] = "get"
libraries[ace][download][url] = "https://github.com/ajaxorg/ace-builds/archive/v1.2.6.zip"
libraries[ace][directory_name] = ace


; Themes
projects[zurb_foundation][type] = "theme"
projects[zurb_foundation][version] = 5.0-rc6


projects[adminimal_theme][type] = "theme"
projects[adminimal_theme][download][type] = "get"
projects[adminimal_theme][download][url] = https://ftp.drupal.org/files/projects/adminimal_theme-7.x-1.24.zip

